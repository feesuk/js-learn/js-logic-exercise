function calculatorConsole(num1, num2, operator) { // operator needs to be in string, ex: "+", "*"
    let result = 0;
    // num1 = parseFloat(num1);
    // num2 = parseFloat(num2);

    if (operator === "+") {
        result = num1 + num2;
    } else if (operator === "-") {
        result = num1 - num2;4
    } else if (operator === "*") {
        result = num1 * num2;
    } else if (operator === "/") {
        result = num1 / num2;
    } else {
        result = "wrong input!"
    }

    return `${num1} ${operator} ${num2} = ${result}`;
}

console.log(calculatorConsole(5, 4, "+"));
console.log(calculatorConsole(5, 4, "*"));
console.log(calculatorConsole(5, 4, "-"));
console.log(calculatorConsole(5, 4, "/"));
console.log(calculatorConsole(5, 4.1, "+"))
console.log(calculatorConsole(2.5, 3, "*"))