let shoppingCart = [
  {
    productID: "FOOD-0001",
    productName: "Roti Buaya",
    price: 75000,
  },
  {
    productID: "BEVERAGE-0005",
    productName: "Coca-cola",
    price: 18000,
  },
  {
    productID: "FOOD-0005",
    productName: "Tahu",
    price: 5000,
  },
  {
    productID: "BEVERAGE-0002",
    productName: "Martabak",
    price: 35000,
  },
  {
    productID: "FOOD-0500",
    productName: "Risol",
    price: 5000,
  },
];

// 1. show FOOD products ONLY
const foodProductOnly = shoppingCart.filter((product) => {
  return product.productID.includes("FOOD");
  // return (
  //   product.productID === "FOOD-0001" ||
  //   product.productID === "FOOD-0005" ||
  //   product.productID === "FOOD-0500"
  // );
});

console.log("1. shoppingCart =", JSON.stringify(foodProductOnly, null, 2));

console.log("---------------------------------------------");

// 2. show details of 'Risol'
const risolDetail = shoppingCart.filter((product) => {
  return product.productName === "Risol";
});

console.log("2. shoppingCart =", JSON.stringify(risolDetail, null, 2));

console.log("---------------------------------------------");

// 3. show products which price is more than 50000
const expensiveProducts = shoppingCart.filter((product) => {
  return product.price > 50000;
});

console.log("3. shoppingCart =", JSON.stringify(expensiveProducts, null, 2));

console.log("---------------------------------------------");

// 4. show all product names (productName) in the shoppingCart
const productNames = shoppingCart.map((product) => {
  return product.productName;
});

console.log(productNames);
