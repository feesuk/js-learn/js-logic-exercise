class Animal {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    introduce() {
        console.log(`Hi! This is ${this.name}.`)
    }
}

class Mutant extends Animal {
    constructor(name, age, superpowers) {
        super(name, age)
        this.superpowers = superpowers;
    }

    introduce() {
        super.introduce(); 
        console.log(`My superpower is`, this.superpowers);
    }

    power() {
        let acak = Math.floor(Math.random() * this.superpowers.length)
        console.log("Power some", this.superpowers[acak]);
    }
}

let Doggy = new Mutant("Mikey", "9", ["flying", "speed"]);
Doggy.introduce(["flying"]);
Doggy.power();

