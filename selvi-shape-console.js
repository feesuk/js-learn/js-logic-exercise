const rectangleWidth = 200;
const rectangleLength = 20;

const rectangleArea = rectangleWidth * rectangleLength;


console.log(`${rectangleWidth} * ${rectangleLength} = ${rectangleArea}`);


const triangleBase = 30;
const triangleHeight = 50;

const triangleArea = 1/2 * ( triangleBase * triangleHeight);

console.log(` 1/2 * ${triangleBase} * ${triangleHeight} = ${triangleArea}`);