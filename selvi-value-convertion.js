let angka = 56;

if (angka >= 0 && angka <= 59){
    console.log("E");
} else if (angka >= 60 && angka <= 69){
    console.log("D");
} else if (angka >= 70 && angka <= 79){
    console.log("C");
} else if (angka >= 80 && angka <= 89){
    console.log("B");
} else if (angka >= 90 && angka <= 100){
    console.log("A");
} else {
    console.log("Tidak diketahui")
}