class Animal {
    constructor(name, age) {
        this.name = name;
        this.age = age;
    }

    introduce() {
        console.log(`Hi! This is ${this.name}.`)
    }
}

class Mutant extends Animal {
    constructor(name, age, superpowers) {
        super(name, age)
        this.superpowers = superpowers;
    }

    introduce(hobby) {
        super.introduce();
        hobby ? console.log(`My hobby is ${hobby}`) : console.log("No Hobby");
    }

    power() {
        let acak = Math.floor(Math.random() * this.superpowers.length)
        console.log("Power some", this.superpowers[acak]);
    }
}

let Doggy = new Mutant("Mikey", "9", ["flying", "speed"]);
Doggy.introduce(["flying"]);
Doggy.introduce("eating");