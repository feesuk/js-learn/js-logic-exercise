class Animal {
    constructor(nama, kaki, isMamalia){
        this.nama = nama;
        this.kaki = kaki;
        this.isMamalia = isMamalia;
    }

    kegiatan(makan){
    makan? console.log(`aku lagi makan ${makan}`) : console.log(`aku sedang tidak makan`)
    }
}

class Mutan extends Animal{
    constructor(nama, kaki, isMamalia, kelebihan){
        super(nama, kaki, isMamalia);
        this.kelebihan = kelebihan;
    }

    kegiatan(makan, minum){
        super.kegiatan(makan);
        minum ? console.log(`aku lagi minum ${minum}`) : console.log(`aku sedang tidak minum`);
    }
}

let animal = new Animal("kucing", "empat", true);
console.log(animal.nama);

let mutan = new Mutan("kucing", "empat", true, "9 nyawa");
console.log(mutan.kelebihan);

mutan.kegiatan("ikan", "susu");