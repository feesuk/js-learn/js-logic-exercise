class Animal {
    constructor(name,spesies) {
        this.name = name;
        this.spesies = spesies;
    }

    introduce() {
        console.log (`hi, gaes i'm ${this.name} and i'm a cute ${this.spesies}`)
    }
}

class Mutant extends Animal {
    constructor (name,spesies,superPowers) {
        super(name,spesies);
        this.superPowers = superPowers;
    }

    introduce (food) {
        super.introduce();
        food ? console.log (`My food is ${food}`) : console.log("not my food");

    }

    power() {
        let acak = Math.floor (Math.random () * this.superPowers.length)
        console.log("Power some", this.superPowers[acak]);
    }
}

let Doggy = new Mutant ("Bruno","poddle",["kungfu","superkick"]);
Doggy.introduce(["kungfu"]);
Doggy.introduce("royal chanin");
Doggy.power();
