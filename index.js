//<---- Instance Constractor dan Method 

class Animal {
    constructor (name, strong, food) {
    this.name = name;
    this.strong = strong;
    this.food = food;
}    

 greet () {
    console.log(`Hai, aku si ${this.name}, makanan kesukaanku adalah ${this.food}, dan aku sangat ${this.strong ? "strong" : "weak"}`);
    } 
}  

let kd = new Animal("Kuda", true, "grass");
let ay = new Animal("Ayam", false, "worm");
kd.greet() 
ay.greet()


//<---- Inheritance ---->
class Mutan extends Animal{
    constructor(name, strong, food, foot) {
        super(name, strong, food)
        this.foot = foot;
    }

    //<--- Overloading --->
    greet(hoby) {
        super.greet();
        hoby ? console.log(`Hoby saya adalah`, hoby) : console.log(`Aku gak punya hoby`)
    }

    //<---- Overriding ---->
    greet() {
        console.log(`Kakiku ada`, this.foot)
    }   
}

let Kuda = new Mutan("Kuda", true, "grass", "4");
Kuda.greet("makan")                                 // Overloading

Kuda.greet()                                        // Overriding


//<---- Static Constructor
class Flower {
    static isFragrant = true;
    static group = "plants";
    constructor(name, isFragrant) {
        this.name = name;
        this.isFragrant = isFragrant;
    }
    hello () {
        console.log(`Aku suka bunga ${this.name} karena baunya sangat ${this.isFragrant ? "wangi" : "bau"}`);
    }
}

let rose = new Flower('rose', true);
rose.hello() 