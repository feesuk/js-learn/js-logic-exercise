// Calculating the area of a triangle
function triangleArea(base, height) {
    let area = (base * height) / 2;
    return `the area of the Triangle is ${area}`;
}
console.log(triangleArea(4, 6)); // return 12

// Calculating the area of a circle
const circleArea = radius => {
    let pi = 3.14;
    let area = pi * Math.pow(radius, 2); // radius pangkat 2
    return `the area of the Circle is ${area}`
}
console.log(circleArea(2));

// console.log(Math.pow(2, 3)); // 2 pangkat 3