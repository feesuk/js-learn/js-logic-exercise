# HOC Excercise

## diketahui :

```js
let shoppingCart = [
  {
    productID: "FOOD-0001",
    productName: "Roti Buaya",
    price: 75000,
  },
  {
    productID: "BEVERAGE-0005",
    productName: "Coca-cola",
    price: 18000,
  },
  {
    productID: "FOOD-0005",
    productName: "Tahu",
    price: 5000,
  },
  {
    productID: "BEVERAGE-0002",
    productName: "Martabak",
    price: 35000,
  },
  {
    productID: "FOOD-0500",
    productName: "Risol",
    price: 5000,
  },
];
```

## Ditanya :

- 1- tampilan hanya produk makanan di dalam shoppingCart
- 2- tampilan data lengkap dari produk 'Risol'
- 3- tampilkan barang-barang belanja yang harganya lebih dari 50000 shoppingCart
- 4- tampilan data nama-nama produk (`productName`) yang ada di dalam shoppingCart

## keluaran yang diharapkan :

- 1

```js
shoppingCart = [
  {
    productID: "FOOD-0001",
    productName: "Roti Buaya",
    price: 75000,
  },
  {
    productID: "FOOD-0005",
    productName: "Tahu",
    price: 5000,
  },
  {
    productID: "FOOD-0500",
    productName: "Risol",
    price: 5000,
  },
];
```

- 2

```js
shoppingCart = {
  productID: "FOOD-0500",
  productName: "Risol",
  price: 5000,
};
```

- 3

```js
shoppingCart =  [{
    {
        productID : 'FOOD-0001',
        productName : 'Roti Buaya',
        price : 75000
    }
}]
```

- 4

```js
shoppingCart =  ['Roti Buaya', 'Coca-cola', 'Tahu','Martabak','Risol'}]
```
