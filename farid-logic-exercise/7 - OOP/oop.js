class Animal {
  
    constructor(name, foodType) {
      this.name = name;
      this.foodType = foodType;
    }
  
    introduce() {
      console.log(`It is ${this.name} speaking, i am ${this.foodType}`)
    }
 }
  
  class Mutan extends Animal {
  
    constructor(name, foodType, superPower) {
      super(name, foodType)
      this.superPower = superPower;
    }
  
    introduce() {
      super.introduce();
      console.log(`i can do some cool things like `, this.superPower);
    }
  
    randomSuperPower() {
      let pickOne = Math.floor(Math.random() * this.superPower.length)
      console.log("Look, i can do ", this.superPower[pickOne])
    }
  
 }
  
  let kodok = new Mutan("Kodok Ngorek", "Omnivore", ["InsaneJump", "StrongPunch", "BigJab", "Invisible", "CrazyFrogMode"]);
  let hiu = new Mutan("Hiu Patil", "Carnivore", ["WaterSpeedster", "Muncher", "JAWSMode", "WaterTornado",]);
  let kancil = new Mutan("Flash Kancil", "Herbivore", ["CucumberStealer", "AntiCrocodile", "Theslashshinging"]);
  
  // hiu.introduce() 
  
  hiu.randomSuperPower()
  
