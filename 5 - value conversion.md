Buatlah aplikasi JS untuk mengkonversi nilai angka ke huruf dengan ketentuan :

| Niali Angka | Nilai Huruf | 
| ------ | ------ |
| A | 90-100 |
| B | 80-89 |
| C | 70-79 |
| D | 60-69 |
| E | 0-59 |
