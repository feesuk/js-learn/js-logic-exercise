// 1. buatlah class yang bernama Animal, yang memiliki atribut dan method

class Animal {
    constructor(food, legs, habitat) {
        this.food = food;
        this.legs = legs;
        this.habitat = habitat;
    }

    livingPlace() {
        return `I live on the ${this.habitat}`;
    }

    static isEating(food) {
        let foods = ["carnivore", "herbivore", "omnivore"];
        return foods.includes(food.toLowerCase());
    }

}

// calling static method
console.log(Animal.isEating("herbivore"));
console.log(Animal.isEating("jashdajkb"));

// making a new instance
let dog = new Animal("meat", 4, "land");
console.log(dog);
console.log(dog.food);
console.log(dog.legs);
console.log(dog.habitat);
console.log(dog.livingPlace());

// 2. buatlah class Mutan yang mana child dari class Animal terapkan kosep overriding atau overloading di class Mutan ini

class Mutan extends Animal {
    constructor(food, legs, habitat, power) {
        super(food, legs, habitat);
        this.power = power;
    }

    livingPlace() {
        super.livingPlace();
        return `I have a superpower called ${this.power} to make people have a second live`;
    }

}

let bear = new Mutan("honey", 4, "land", "Pamparampam");
console.log(bear);
console.log(bear.food);
console.log(bear.legs);
console.log(bear.habitat);
console.log(bear.livingPlace());

