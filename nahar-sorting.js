// input => [1, 3, 104, 2, 5, 1, 2, 6, 140000, 99]
// expected output => [1, 1, 2, 2, 3, 5, 6, 99, 104, 140000]
let number = [1, 3, 104, 2, 5, 1, 2, 6, 140000, 99];


//normally, the sort instruction will only sort the data alphabetically. 
//to change it into sorting number, we need to create a function which will sort it numerically. 
number.sort(function(a, b) {
    return a - b;
  });
  console.log(number);