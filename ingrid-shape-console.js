const rectangleWidth = 70;
const rectangleLength = 10;

const rectangleArea = rectangleWidth * rectangleLength;

console.log(`${rectangleArea} = ${rectangleWidth} * ${rectangleLength}`);
