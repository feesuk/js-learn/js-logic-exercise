// input => [1, 3, 104, 2, 5, 1, 2, 6, 140000, 99]
// expected output => [1, 1, 2, 2, 3, 5, 6, 99, 104, 140000]

// the easiest way would be using a built-in function lol
const sortArr = (arr) => {
    return arr.sort((a, b) => a - b);
}

console.log(sortArr([1, 3, 104, 2, 5, 1, 2, 6, 140000, 99]));
console.log(sortArr([5, 2, 1, 0, 4]));


