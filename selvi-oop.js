class Animal {
    constructor(nama, kaki, isMamalia){
        this.nama = nama
        this.kaki = kaki
        this.isMamalia = isMamalia
    }

    kegiatan(makan) {
        makan ? console.log(`Aku lagi makan ${makan}`) : console.log(`saya tidak sedang makan`)
    }
}

class Mutan extends Animal {
    constructor(nama, kaki, isMamalia, kelebihan) {
        super(nama, kaki, isMamalia)
        this.kelebihan = kelebihan
    }

    kegiatan(makan, minum){
        super.kegiatan(makan)
        minum ? console.log(`Aku lagi minum  ${minum}`) : console.log(`Saya tidak minum`)
    }

}

let animal = new Animal ('kucing', 'empat', true);
console.log(animal.nama);
let mutan = new Mutan('kucing', 'empat', true, '9 nyawa')
console.log(mutan)

// animal.kegiatan('ikan')
mutan.kegiatan('ikan', 'susu')
mutan.kegiatan('', '')