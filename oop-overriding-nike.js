class Animal {
    constructor(name,spesies) {
        this.name = name;
        this.spesies = spesies;
    }

    introduce(){
        console.log(`hello, my name is ${this.name}`)
    }
}

class Mutant extends Animal {
    constructor(name, spesies, superPowers){
        super(name, spesies);
        this.superPowers = superPowers;
    }

    introduce(){
        super.introduce();
        console.log(`I am a ${this.spesies} and I have ${this.superPowers} skill`);
    }

    power() {
      console.log(
          "Power some",
          this.superPowers[
              Math.floor(Math.random() * this.superPowers.length)
          ]
      )  
    }
}

let Doggy = new Mutant ("Mikey","bulldog",["flying","laser eye"]);
Doggy.introduce(["flying"]);
Doggy.power();

let tiger= new Mutant ("Miko","benggala",["high jump","superspeed"]);
tiger.introduce("high jump");
tiger.power();
