//membuat class yang bernama animal, yang memiliki atribut dan method
class Animal {
    constructor (name, strong, food) {
    this.name = name;
    this.strong = strong;
    this.food = food;
}    
    ability () {
        return `Hey, I am a ${this.name}, my favorite food is ${this.food}, and I am very ${this.strong ? "strong" : "weak"}`;
    } 
    static isFondOf(food) {
        let foods = ["kabehpurun", "herbivore", "omnivore"];
        return foods.includes(food.toLowerCase());
    }
}  
//printing out the static method
console.log(Animal.isFondOf("kabehpurun")); //it returns true
console.log(Animal.isFondOf("telo")); //it returns false

//printing out the instance method (creating the variable before printing/calling it)
let kd = new Animal("horse", true, "grass");
let ay = new Animal("chick", false, "worm");
let kp = new Animal("butterfly", false, "polen");
console.log(kd.ability()); //------->Hey, I am a horse, my favorite food is grass, and I am very strong
console.log(ay.ability()); //------->Hey, I am a chick, my favorite food is worm, and I am very weak
console.log(kp.ability()); //------->Hey, I am a butterfly, my favorite food is polen, and I am very weak
console.log(kd.food); //------------>grass
console.log(ay.strong); //---------->false
console.log(kp.name); //------------>butterfly




//Overriding/Overloading
class Mutan extends Animal{
    constructor(name, strong, food, foot) {
        super(name, strong, food)
        this.foot = foot;
    }

    power() {
        super.power();
        return `I have 6 ${this.foot} and I am proud of it`;
        // console.log (`I haveI have ${foot} foot`)
    }    
}
let tiger = new Mutan("simba","invisible","human","small");
console.log(tiger);
console.log(tiger.name);
console.log(tiger.strong)