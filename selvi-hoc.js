let shoppingCart = [
  {
    productID: "FOOD-0001",
    productName: "Roti Buaya",
    price: 75000,
  },
  {
    productID: "BEVERAGE-0005",
    productName: "Coca-cola",
    price: 18000,
  },
  {
    productID: "FOOD-0005",
    productName: "Tahu",
    price: 5000,
  },
  {
    productID: "BEVERAGE-0002",
    productName: "Martabak",
    price: 35000,
  },
  {
    productID: "FOOD-0500",
    productName: "Risol",
    price: 5000,
  },
];

let justFoods = shoppingCart.filter((item) => item.productID.includes("FOOD"));
console.log(justFoods);

let justRisol = shoppingCart.filter((item) => item.productName === "Risol");
console.log(justRisol);

let justFifty = shoppingCart.filter((item) => item.price > 50000);
console.log(justFifty);

let justName = shoppingCart.map((item) => item.productName);
console.log(justName);
