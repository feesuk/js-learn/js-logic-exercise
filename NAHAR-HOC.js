let shoppingCart = [
	{
		productID: "FOOD-0001",
		productName: "Roti Buaya",
		price: 75000,
	},
	{
		productID: "BEVERAGE-0005",
		productName: "Coca-cola",
		price: 18000,
	},
	{
		productID: "FOOD-0005",
		productName: "Tahu",
		price: 5000,
	},
	{
		productID: "BEVERAGE-0002",
		productName: "Martabak",
		price: 35000,
	},
	{
		productID: "FOOD-0500",
		productName: "Risol",
		price: 5000,
	},
];

// 1- tampilan hanya produk makanan di dalam shoppingCart
// 2- tampilan data lengkap dari produk 'Risol'
// 3- tampilkan barang-barang belanja yang harganya lebih dari 50000 shoppingCart
// 4- tampilan data nama-nama produk (productName) yang ada di dalam shoppingCart

// ===================> JAWABAN <==================\\
// 1
const foodCart = shoppingCart.filter(item => item.productID.includes("FOOD"));
console.log(foodCart);

// 2
const risolFood = shoppingCart.find(
	risolItem => risolItem.productName === "Risol"
);
console.log(risolFood);

// 3
const pricyProduct = shoppingCart.filter(pricy => pricy.price > 50000);
console.log(pricyProduct);

// 4
const nameOfProduct = shoppingCart.map(stuff => stuff.productName);
console.log(nameOfProduct);
