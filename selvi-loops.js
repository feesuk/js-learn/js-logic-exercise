let iteration = 1;

do {
    console.log(iteration);
    iteration += 25;
} while (iteration < 200);

const numbers = [2, 3, 5, 7, 11, 13, 17, 19, 23];

for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    console.log(element);
}

const names = ["Selvi", "Nike", "Yuli", "Inggrid", "Riri"];

for (let index = 0; index < names.length; index++) {
    const element = names[index];
    console.log(element);
}

let num = 4;
let total = 1;


for (i = 0; i < num; i++){
    total = total * (num - i);
}
console.log(`${num}! = ${total}`)


// console.log(`${num}! = ${total}`)=

reverseName = "Selviany";
let reversedString = "";

for (let i = reverseName.length - 1; i >= 0; i--){
    reversedString = reversedString + reverseName[i] 
}
console.log(reversedString);
