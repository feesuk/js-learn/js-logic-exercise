//Example 1: defining odd numbers
function oddNum(min, max) {
    let myNum = [];
    for (let i = min; i <= max; i++) {
        if (i%2 !== 0) {
            myNum.push(i);
        }
    }
    return myNum;
}
console.log(oddNum(1, 9)); // return [ 1, 3, 5, 7, 9 ]


// Example 2: summing array elements
// try using ES6
const sum = arr => {
    let addition = 0;
    let i = 0;
    while (i < arr.length) {
        addition += arr[i];
        i++;
    }
    return addition;
}

console.log(sum([3, 2, 1, 2, 3])) // return 11

// Example 3: looping through all array elements in reverse order
let myArr = ["go!", "one", "two", "three", "four", "five", "here we go!", "reverse order"];
for (let i = myArr.length - 1; i >= 0; i--) {
    console.log(myArr[i]);
}