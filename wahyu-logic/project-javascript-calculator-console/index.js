const digitA = 222;
const digitB = 333;

const hasilPenjumlahan = digitA + digitB;
const hasilPengurangan = digitA - digitB;
const hasilPerkalian = digitA * digitB;
const hasilPembagian = digitA / digitB;

console.log("digitA = " + digitA);
console.log("digitB = " + digitB);

console.log(`Hasil dari ${digitA} + ${digitB} = ${hasilPenjumlahan}`);
console.log(`Hasil dari ${digitA} - ${digitB} = ${hasilPengurangan}`);
console.log(`Hasil dari ${digitA} * ${digitB} = ${hasilPerkalian}`);
console.log(`Hasil dari ${digitA} / ${digitB} = ${hasilPembagian}`);
